import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { FileSelectorComponent } from './file-selector/file-selector.component';
import { CsvViewerComponent } from './csv-viewer/csv-viewer.component';
import { ngfModule } from 'angular-file';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule, MatFormFieldModule, MatSortModule } from '@angular/material';
import { ToastrModule } from 'ngx-toastr';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        MatTableModule,
        MatSortModule,
        MatFormFieldModule,
        ngfModule,
        ToastrModule.forRoot()
      ],
      declarations: [
        AppComponent,
        FileSelectorComponent,
        CsvViewerComponent
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create the app', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should set showTableComponent when csvObject exists', () => {
    expect(component.showTableComponent).toBeFalsy();

    // null value should set it to false
    component.csvObjectChange(null);
    expect(component.showTableComponent).toBe(false);

    // sending an array should set it to true
    component.csvObjectChange([]);
    expect(component.showTableComponent).toBe(true);
  });
});
