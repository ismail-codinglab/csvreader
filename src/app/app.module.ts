// Angular modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule, MatSortModule, MatFormFieldModule} from '@angular/material';

// Components
import { AppComponent } from './app.component';

// Plugins
import { ngfModule } from 'angular-file';
import { ToastrModule } from 'ngx-toastr';
import { FileSelectorComponent } from './file-selector/file-selector.component';
import { CsvViewerComponent } from './csv-viewer/csv-viewer.component';

@NgModule({
  declarations: [
    AppComponent,
    FileSelectorComponent,
    CsvViewerComponent
  ],
  imports: [
    // Angular modules
    BrowserModule,
    BrowserAnimationsModule,

    // Angular Material modules
    MatTableModule,
    MatSortModule,
    MatFormFieldModule,

    // Plugins
    ngfModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
