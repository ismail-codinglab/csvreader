import { Component, OnInit, ViewChild } from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import { CsvService } from '../csvService/csv.service';
import { Subscription } from 'rxjs';
import { IssueElement } from '../csvService/IssueElement';

@Component({
  selector: 'app-csv-viewer',
  templateUrl: './csv-viewer.component.html',
  styleUrls: ['./csv-viewer.component.scss']
})
export class CsvViewerComponent implements OnInit {

  constructor(
    private csvService: CsvService
  ) { }

  csvObject$: Subscription;
  displayedColumns: string[] = ['firstName', 'surName', 'issueCount', 'dateOfBirth'];
  dataSource = null;

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.csvObject$ = this.csvService.getCsvObject().subscribe(this.csvObjectChange.bind(this));
  }

  csvObjectChange(csvData: IssueElement[]) {
    this.dataSource = new MatTableDataSource(csvData);
    this.dataSource.sort = this.sort;
  }

  removeFile() {
    this.csvService.removeFile();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
