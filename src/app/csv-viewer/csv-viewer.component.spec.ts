import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsvViewerComponent } from './csv-viewer.component';
import { MatTableModule, MatSortModule, MatFormFieldModule, MatTableDataSource } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CsvService } from '../csvService/csv.service';

describe('CsvViewerComponent', () => {
  let component: CsvViewerComponent;
  let fixture: ComponentFixture<CsvViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsvViewerComponent ],
      imports: [
        BrowserAnimationsModule,
        MatTableModule,
        MatSortModule,
        MatFormFieldModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsvViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the datasource', () => {
    expect(component.dataSource).toBeNull();
    component.csvObjectChange([]);
    expect(component.dataSource).not.toBeNull();
  });

  it('should set the filter', () => {
    // Initial value
    component.csvObjectChange([]); // set the datasource
    expect(component.dataSource.filter).toBe('');

    // lowercase string
    component.applyFilter('test'); // set a filter
    expect(component.dataSource.filter).toBe('test');

    // mixedcase string
    component.applyFilter('MiXeDCaSe'); // set a filter
    expect(component.dataSource.filter).toBe('mixedcase');

    // string with spaces
    component.applyFilter(' spaces '); // set a filter
    expect(component.dataSource.filter).toBe('spaces');
  });
});
