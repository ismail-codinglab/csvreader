import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileSelectorComponent } from './file-selector.component';
import { ngfModule } from 'angular-file';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { CsvService } from '../csvService/csv.service';

describe('FileSelectorComponent', () => {
  let component: FileSelectorComponent;
  let fixture: ComponentFixture<FileSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileSelectorComponent ],
      imports: [ ngfModule, ToastrModule.forRoot() ],
      providers: [
        { provide: ToastrService, useValue: {} }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the csvObject in service', () => {
    const service: CsvService = TestBed.get(CsvService);
    const testFile = new File([], '');
    component.fileChange(testFile);

    service.getCsvObject().subscribe(result => expect(result).not.toBeNull());
  });

  it('should call the toastr warning method', () => {
    const service = TestBed.get(ToastrService); // get your service
    service.warning = () => {};

    const testFile = new File([], '');
    component.invalidFileChange([{file: testFile, type: 'accept'}]);
    expect(service.warning.toHaveBeenCalled);
  });
});
