import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CsvService } from '../csvService/csv.service';

@Component({
  selector: 'app-file-selector',
  templateUrl: './file-selector.component.html',
  styleUrls: ['./file-selector.component.scss']
})
export class FileSelectorComponent implements OnInit {

  validComboDrag: boolean;
  maxSizeInMbs = 1;
  maxSize: number = 1024 * 1024 * this.maxSizeInMbs; // 1MB

  constructor(
    private toastrService: ToastrService,
    private csvService: CsvService
  ) { }

  ngOnInit() { }

  fileChange(file: File) {
    this.csvService.useFile(file);
  }

  invalidFileChange(files: {file: File, type: 'accept' | 'fileSize'}[]) {
    if (!files || !files.length) { // No action required if there are no invalid files
      return;
    }

    const lastChangedFile = files.pop();
    const lastChangedFileName = lastChangedFile.file.name;

    switch (lastChangedFile.type) {
      case 'accept':
        this.toastrService.warning(`Het bestand ${lastChangedFileName} is niet van het juiste bestandsformaat`);
        break;
      case 'fileSize':
        this.toastrService.warning(`Het bestand ${lastChangedFileName} is te groot. De maximale grootte is ${this.maxSizeInMbs}MB.`);
        break;
    }
  }
}
