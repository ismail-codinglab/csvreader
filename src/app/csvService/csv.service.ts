import { Injectable } from '@angular/core';
import { IssueElement } from './IssueElement';
import { Subject, Observable, ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CsvService {

  private csvObject$: Subject<IssueElement[]> = new ReplaySubject(1);

  constructor() { }

  useFile(file: File) {
    this.extractData(file);
  }

  removeFile() {
    this.csvObject$.next(null);
  }

  getCsvObject(): Observable<IssueElement[]> {
    return this.csvObject$;
  }

  extractData(file: File) {
    const fileReader = new FileReader();
    fileReader.onload = () => {
      this.csvObject$.next(this.convertCsvToKeyValuePair(fileReader.result as string));
    };
    fileReader.readAsText(file);
  }

  convertCsvToKeyValuePair(csvString: string): IssueElement[] {
    const lines = csvString.split('\n');

    const result: IssueElement[] = [];

    for (let i = 1; /* skip header */ i < lines.length; i++) {

      const currentLine = lines[i].split(',');
      const issue: IssueElement = {
        firstName: this.removeSurroundingQuotes(currentLine[0]),
        surName: this.removeSurroundingQuotes(currentLine[1]),
        issueCount: Number(currentLine[2]),
        dateOfBirth: this.removeSurroundingQuotes(currentLine[3].trim() /* removes whitespace at the end of the line */)
      };

      result.push(issue);
    }

    return result;
  }

  removeSurroundingQuotes(text: string): string { // only remove surrounding quotes, keep in-text quotes untouched
    return text.replace(/^"(.*)"$/, '$1');
  }
}
