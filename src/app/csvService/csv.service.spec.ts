import { TestBed } from '@angular/core/testing';

import { CsvService } from './csv.service';
import { IssueElement } from './IssueElement';

describe('CsvService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CsvService = TestBed.get(CsvService);
    expect(service).toBeTruthy();
  });

  it('should remove surrounding quotes', () => {
    const service: CsvService = TestBed.get(CsvService);

    // simple text
    expect(service.removeSurroundingQuotes('"John"')).toEqual('John');
    // text with single quotes inside
    expect(service.removeSurroundingQuotes(`"Don't"`)).toEqual(`Don't`);
    // text with double quotes inside
    expect(service.removeSurroundingQuotes(`"a "quote""`)).toEqual(`a "quote"`);
  });

  it('should return an array of issue elements', () => {
    const service: CsvService = TestBed.get(CsvService);
    // input
    const testCsvString = `"First name","Sur name","Issue count","Date of birth"
"John","Doe",12,"1900-12-08T00:00:00"`;
    // expected output
    const testCsvKeyValuePair: IssueElement[] = [
      {firstName: 'John', surName: 'Doe', issueCount: 12, dateOfBirth: '1900-12-08T00:00:00'},
    ];

    expect(service.convertCsvToKeyValuePair(testCsvString)).toEqual(testCsvKeyValuePair);
  });

  it('should remove the csv file', () => {
    const service: CsvService = TestBed.get(CsvService);
    service.getCsvObject().subscribe(result => expect(result).toBe(null));
    service.removeFile();
  });
});
