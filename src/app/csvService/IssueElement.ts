export interface IssueElement {
  firstName: string;
  surName: string;
  issueCount: number;
  dateOfBirth: string;
}
