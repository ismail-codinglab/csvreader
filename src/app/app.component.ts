import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { CsvService } from './csvService/csv.service';
import { Subscription } from 'rxjs';
import { IssueElement } from './csvService/IssueElement';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  csvObject$: Subscription;
  showTableComponent: boolean;

  constructor(
    private csvService: CsvService
  ) { }

  ngOnInit() {
    this.csvObject$ = this.csvService.getCsvObject().subscribe(this.csvObjectChange.bind(this));
  }

  csvObjectChange(csvData: IssueElement[]) {
    this.showTableComponent = csvData != null;
  }

  ngOnDestroy() {
    this.csvObject$.unsubscribe();
  }
}
