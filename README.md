# CSV Reader

This is the CSV Reader I have developed for Rabobank to showcase my coding style, skills and knowledge. Since the challenge was not detailed there was room for making assumptions. For example, should it only be able to accept a very specific csv file with a specific format or support different formats?

### Tools used

 * Visual Studio Code
   * Tslint
 * Angular CLI
   * Creating app
   * Creating components
   * Creating service
 * Angular 7
   * Toastr - For displaying error messages 
   * Angular-file - Being able to import csv files through drag & drop and selecting a file
   * RxJS - Reactive programming, making use of Observables and Subscriptions.
   * Angular Material - Used to display, filter and sort a table 
 * Git
   * Git flow - A standard for using git in a team or for production 
 * Bootstrap - creating a html structure & simplistic styling
 * Scscc - allows me to nest css classes, re-use them and use variables if needed


## Making it production-ready

To make this project production-ready I would suggest putting the build files into a docker container and label a version number to it. This docker container will get deployed and use kubernetes to enable horizontal scaling.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
